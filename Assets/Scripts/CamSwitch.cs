﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CamSwitch : MonoBehaviour
{
    public GameObject Cam1;
    public GameObject Cam2;
    public GameObject Cam3;
    public GameObject Cam4;
    public GameObject Cam5;
    public GameObject Cam6;

    public GameObject SubCamera;

    // Update is called once per frame
    void Update()
    {

        if (SubCamera.activeSelf == false)
        {

            if (Input.GetButtonDown(("1Key")))
            {
                Cam1.SetActive(true);
                Cam2.SetActive(false);
                Cam3.SetActive(false);
                Cam4.SetActive(false);
                Cam5.SetActive(false);
                Cam6.SetActive(false);
            }
            if (Input.GetButtonDown("2Key"))
            {
                Cam1.SetActive(false);
                Cam2.SetActive(true);
                Cam3.SetActive(false);
                Cam4.SetActive(false);
                Cam5.SetActive(false);
                Cam6.SetActive(false);
            }
            if (Input.GetButtonDown("3Key"))
            {
                Cam1.SetActive(false);
                Cam2.SetActive(false);
                Cam3.SetActive(true);
                Cam4.SetActive(false);
                Cam5.SetActive(false);
                Cam6.SetActive(false);
            }
            if (Input.GetButtonDown("4Key"))
            {
                Cam1.SetActive(false);
                Cam2.SetActive(false);
                Cam3.SetActive(false);
                Cam4.SetActive(true);
                Cam5.SetActive(false);
                Cam6.SetActive(false);
            }
            if (Input.GetButtonDown("5Key"))
            {
                Cam1.SetActive(false);
                Cam2.SetActive(false);
                Cam3.SetActive(false);
                Cam4.SetActive(false);
                Cam5.SetActive(true);
                Cam6.SetActive(false);
            }
            if (Input.GetButtonDown("6Key"))
            {
                Cam1.SetActive(false);
                Cam2.SetActive(false);
                Cam3.SetActive(false);
                Cam4.SetActive(false);
                Cam5.SetActive(false);
                Cam6.SetActive(true);
            }

        }

        if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene("WalkModelHouse");
        }
    }
}
