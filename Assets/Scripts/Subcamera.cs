﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Subcamera : MonoBehaviour
{
    public GameObject MainCamera;
    public GameObject GameObject;
    public GameObject Canvas;

    //移動用
    public float speed;
    public float updownSpeed;
    public float rotateSpeed;
    Vector3 startPos; //初期位置

    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = MainCamera.transform.position;

        startPos = this.transform.position;


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.back * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            transform.Translate(Vector3.up * Time.deltaTime * updownSpeed);
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(Vector3.down * Time.deltaTime * updownSpeed);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.down * Time.deltaTime * rotateSpeed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up * Time.deltaTime * rotateSpeed);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            transform.Translate(Vector3.up * Time.deltaTime * updownSpeed);
        }
       
        if (Input.GetKey("r"))
        {
            MainCamera.SetActive(true);
            this.GameObject.SetActive(false);

            Canvas.SetActive(true);
        }
    }
}
