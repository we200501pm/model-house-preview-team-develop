﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WalkPlayer : MonoBehaviour {

    public float speed;
    public float updownSpeed;
    public float rotateSpeed;
    Vector3 startPos; //初期位置

    public GameObject MainCamera;
    public GameObject SubCamera;
    public GameObject Cam1;
    public GameObject Cam2;
    public GameObject Cam3;
    public GameObject Cam4;
    public GameObject Cam5;
    public GameObject Cam6;
    public GameObject Canvas;

    void Start () {

        startPos = transform.position; //初期位置を格納
    }

    void Update () {
        if (Input.GetKey (KeyCode.UpArrow)) {
            transform.Translate (Vector3.forward * Time.deltaTime * speed);
        }
        if (Input.GetKey (KeyCode.DownArrow)) {
            transform.Translate (Vector3.back * Time.deltaTime * speed);
        }
        if (Input.GetKey (KeyCode.Space)) {
            transform.Translate (Vector3.up * Time.deltaTime * updownSpeed);
        }
        if (Input.GetKey (KeyCode.LeftShift)) {
            transform.Translate (Vector3.down * Time.deltaTime * updownSpeed);
        }
        if (Input.GetKey (KeyCode.LeftArrow)) {
            transform.Rotate (Vector3.down * Time.deltaTime * rotateSpeed);
        }
        if (Input.GetKey (KeyCode.RightArrow)) {
            transform.Rotate (Vector3.up * Time.deltaTime * rotateSpeed);
        }
        if (Input.GetKey ("q")) {
            SceneManager.LoadScene ("LookAroundScene");
        }
        if (Input.GetKey (KeyCode.Alpha0)) {
            transform.position = startPos; //初期位置に戻す
        }
        if (Input.GetKey("e"))
        {
            MainCamera.SetActive(false);

            Cam1.SetActive(false);
            Cam2.SetActive(false);
            Cam3.SetActive(false);
            Cam4.SetActive(false);
            Cam5.SetActive(false);
            Cam6.SetActive(false);
            SubCamera.SetActive(true);

            Canvas.SetActive(false);
        }
    }
}