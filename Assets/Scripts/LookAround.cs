﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LookAround : MonoBehaviour {

    public float rotateSpeed;
    public Camera camera;

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKey (KeyCode.LeftArrow)) {
            transform.Rotate (Vector3.down * Time.deltaTime * rotateSpeed);
        }
        if (Input.GetKey (KeyCode.RightArrow)) {
            transform.Rotate (Vector3.up * Time.deltaTime * rotateSpeed);
        }
        if (Input.GetKey (KeyCode.DownArrow)) {
            camera.fieldOfView = camera.fieldOfView + Time.deltaTime * 30;
        }
        if (Input.GetKey (KeyCode.UpArrow)) {
            camera.fieldOfView = camera.fieldOfView - Time.deltaTime * 30;
        }
        if (Input.GetKey ("w")) {
            SceneManager.LoadScene ("WalkModelHouse"); 
        }
        if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene("WalkModelHouse");
        }
    }
}